'use strict';
let hand$x$static = 400;
function hand$main() {
   return graphics$p3$m00m00(function() {
      return hand$setup();
   }, function() {
      return hand$draw();
   });
}
function hand$setup() {
   graphics$size$II(800, 750);
}
function hand$draw() {
   graphics$background$III(200, 200, 200);
   graphics$noStroke();
   graphics$fill$III(80, 80, 80);
   graphics$rect$IIII(0, 590, 800, 160);
   graphics$fill$III(185, 138, 107);
   graphics$triangle$IIIIII(800, 750, 800, 693, 709, 693);
   graphics$fill$III(219, 181, 151);
   graphics$triangle$IIIIII(800, 615, 800, 693, 709, 693);
   graphics$fill$III(223, 187, 157);
   graphics$triangle$IIIIII(800, 615, 670, 606, 709, 693);
   graphics$fill$III(226, 201, 171);
   graphics$triangle$IIIIII(800, 615, 670, 606, 762, 565);
   graphics$fill$III(222, 186, 148);
   graphics$triangle$IIIIII(800, 615, 800, 536, 762, 565);
   graphics$fill$III(199, 149, 115);
   graphics$triangle$IIIIII(736, 504, 800, 536, 762, 565);
   graphics$fill$III(198, 160, 130);
   graphics$triangle$IIIIII(592, 626, 670, 606, 709, 693);
   graphics$fill$III(237, 203, 170);
   graphics$triangle$IIIIII(670, 606, 762, 565, 648, 501);
   graphics$fill$III(213, 169, 133);
   graphics$triangle$IIIIII(736, 504, 762, 565, 648, 501);
   graphics$fill$III(198, 147, 111);
   graphics$triangle$IIIIII(736, 504, 648, 501, 663, 470);
   graphics$fill$III(197, 131, 100);
   graphics$triangle$IIIIII(613, 420, 648, 501, 663, 470);
   graphics$fill$III(209, 173, 142);
   graphics$triangle$IIIIII(592, 626, 670, 606, 545, 575);
   graphics$fill$III(234, 201, 172);
   graphics$triangle$IIIIII(648, 501, 670, 606, 545, 575);
   graphics$fill$III(192, 153, 122);
   graphics$triangle$IIIIII(592, 626, 545, 575, 455, 569);
   graphics$fill$III(224, 190, 158);
   graphics$triangle$IIIIII(522, 456, 545, 575, 455, 569);
   graphics$fill$III(229, 194, 162);
   graphics$triangle$IIIIII(522, 456, 545, 575, 648, 501);
   graphics$fill$III(216, 176, 145);
   graphics$triangle$IIIIII(522, 456, 613, 420, 648, 501);
   graphics$fill$III(222, 183, 152);
   graphics$triangle$IIIIII(522, 456, 613, 420, 529, 363);
   graphics$fill$III(206, 153, 123);
   graphics$triangle$IIIIII(535, 292, 613, 420, 529, 363);
   graphics$fill$III(219, 174, 142);
   graphics$triangle$IIIIII(535, 292, 485, 316, 529, 363);
   graphics$fill$III(208, 152, 119);
   graphics$triangle$IIIIII(535, 292, 485, 316, 493, 238);
   graphics$fill$III(229, 193, 159);
   graphics$triangle$IIIIII(522, 456, 464, 385, 529, 363);
   graphics$fill$III(230, 195, 163);
   graphics$triangle$IIIIII(485, 316, 464, 385, 529, 363);
   graphics$fill$III(232, 195, 159);
   graphics$triangle$IIIIII(485, 316, 464, 385, 438, 341);
   graphics$fill$III(219, 168, 132);
   graphics$triangle$IIIIII(357, 402, 464, 385, 438, 341);
   graphics$fill$III(206, 150, 118);
   graphics$triangle$IIIIII(357, 402, 398, 339, 382, 383);
   graphics$fill$III(234, 198, 163);
   graphics$triangle$IIIIII(485, 316, 438, 341, 414, 294);
   graphics$fill$III(213, 159, 125);
   graphics$triangle$IIIIII(382, 383, 438, 341, 414, 294);
   graphics$fill$III(236, 202, 171);
   graphics$triangle$IIIIII(485, 316, 414, 294, 454, 236);
   graphics$fill$III(233, 194, 161);
   graphics$triangle$IIIIII(430, 213, 414, 294, 454, 236);
   graphics$fill$III(212, 173, 144);
   graphics$triangle$IIIIII(430, 213, 414, 294, 395, 270);
   graphics$fill$III(215, 168, 137);
   graphics$triangle$IIIIII(448, 219, 485, 316, 493, 238);
   graphics$fill$III(219, 178, 151);
   graphics$triangle$IIIIII(422, 212, 368, 225, 313, 275);
   graphics$fill$III(204, 151, 122);
   graphics$triangle$IIIIII(422, 212, 368, 225, 326, 197);
   graphics$fill$III(228, 186, 161);
   graphics$triangle$IIIIII(284, 213, 368, 225, 326, 197);
   graphics$fill$III(211, 157, 131);
   graphics$triangle$IIIIII(284, 213, 251, 190, 326, 197);
   graphics$fill$III(228, 186, 161);
   graphics$triangle$IIIIII(284, 213, 251, 190, 202, 264);
   graphics$fill$III(241, 196, 167);
   graphics$triangle$IIIIII(284, 213, 269, 292, 202, 264);
   graphics$fill$III(210, 136, 103);
   graphics$triangle$IIIIII(284, 213, 269, 292, 290, 273);
   graphics$fill$III(220, 150, 115);
   graphics$triangle$IIIIII(284, 213, 325, 297, 290, 273);
   graphics$fill$III(242, 192, 162);
   graphics$triangle$IIIIII(284, 213, 368, 225, 313, 275);
   graphics$fill$III(241, 193, 161);
   graphics$triangle$IIIIII(197, 308, 269, 292, 202, 264);
   graphics$fill$III(221, 171, 139);
   graphics$triangle$IIIIII(197, 308, 142, 354, 202, 264);
   graphics$fill$III(242, 188, 157);
   graphics$triangle$IIIIII(197, 308, 142, 354, 220, 349);
   graphics$fill$III(223, 142, 110);
   graphics$triangle$IIIIII(197, 308, 269, 292, 220, 349);
   graphics$fill$III(214, 164, 134);
   graphics$triangle$IIIIII(357, 402, 398, 339, 369, 336);
   graphics$fill$III(206, 147, 113);
   graphics$triangle$IIIIII(357, 402, 341, 332, 369, 336);
   graphics$fill$III(183, 114, 81);
   graphics$triangle$IIIIII(357, 402, 341, 332, 333, 385);
   graphics$fill$III(176, 110, 84);
   graphics$triangle$IIIIII(320, 313, 341, 332, 333, 385);
   graphics$fill$III(177, 113, 83);
   graphics$triangle$IIIIII(320, 313, 341, 332, 398, 339);
   graphics$fill$III(187, 134, 106);
   graphics$triangle$IIIIII(320, 313, 378, 301, 398, 339);
   graphics$fill$III(187, 127, 98);
   graphics$triangle$IIIIII(414, 294, 378, 301, 398, 339);
   graphics$fill$III(197, 145, 114);
   graphics$triangle$IIIIII(414, 294, 378, 301, 396, 270);
   graphics$fill$III(177, 123, 95);
   graphics$triangle$IIIIII(354, 292, 378, 301, 396, 270);
   graphics$fill$III(136, 69, 51);
   graphics$triangle$IIIIII(354, 292, 378, 301, 320, 313);
   graphics$fill$III(162, 86, 64);
   graphics$triangle$IIIIII(320, 313, 309, 337, 252, 350);
   graphics$fill$III(208, 155, 124);
   graphics$triangle$IIIIII(318, 389, 309, 337, 252, 350);
   graphics$fill$III(188, 118, 88);
   graphics$triangle$IIIIII(234, 367, 252, 350, 318, 389);
   graphics$fill$III(193, 136, 108);
   graphics$triangle$IIIIII(320, 313, 309, 337, 318, 389);
   graphics$fill$III(187, 134, 106);
   graphics$triangle$IIIIII(320, 313, 333, 385, 318, 389);
   graphics$fill$III(200, 120, 89);
   graphics$triangle$IIIIII(168, 420, 175, 469, 210, 440);
   graphics$fill$III(207, 145, 115);
   graphics$triangle$IIIIII(168, 420, 175, 469, 158, 463);
   graphics$fill$III(208, 162, 142);
   graphics$triangle$IIIIII(168, 420, 153, 459, 158, 463);
   graphics$fill$III(201, 154, 142);
   graphics$triangle$IIIIII(168, 420, 153, 459, 161, 418);
   graphics$fill$III(191, 136, 122);
   graphics$triangle$IIIIII(154, 413, 153, 459, 161, 418);
   graphics$fill$III(201, 156, 140);
   graphics$triangle$IIIIII(201, 438, 200, 485, 191, 450);
   graphics$fill$III(208, 166, 151);
   graphics$triangle$IIIIII(201, 438, 200, 485, 210, 471);
   graphics$fill$III(206, 152, 121);
   graphics$triangle$IIIIII(221, 486, 200, 485, 210, 471);
   graphics$fill$III(204, 131, 98);
   graphics$triangle$IIIIII(221, 486, 247, 451, 210, 471);
   graphics$fill$III(199, 118, 90);
   graphics$triangle$IIIIII(201, 438, 247, 451, 210, 471);
   graphics$stroke$III(59, 132, 240);
   graphics$strokeWeight$I(40);
   graphics$line$IIII(822, 8, -2, 471);
   graphics$stroke$III(36, 81, 148);
   graphics$strokeWeight$I(18);
   graphics$line$IIII(822, 21, -2, 483);
   graphics$noStroke();
   graphics$fill$III(230, 196, 164);
   graphics$triangle$IIIIII(522, 456, 404, 463, 455, 569);
   graphics$fill$III(234, 201, 172);
   graphics$triangle$IIIIII(522, 456, 404, 463, 464, 385);
   graphics$fill$III(223, 179, 144);
   graphics$triangle$IIIIII(404, 463, 464, 385, 357, 402);
   graphics$fill$III(214, 178, 144);
   graphics$triangle$IIIIII(404, 463, 455, 569, 384, 523);
   graphics$fill$III(214, 173, 138);
   graphics$triangle$IIIIII(404, 463, 317, 474, 384, 523);
   graphics$fill$III(235, 197, 169);
   graphics$triangle$IIIIII(404, 463, 317, 474, 357, 402);
   graphics$fill$III(234, 201, 172);
   graphics$triangle$IIIIII(317, 474, 357, 402, 290, 422);
   graphics$fill$III(210, 150, 120);
   graphics$triangle$IIIIII(260, 372, 357, 402, 290, 422);
   graphics$fill$III(220, 176, 146);
   graphics$triangle$IIIIII(317, 474, 223, 446, 290, 422);
   graphics$fill$III(231, 184, 153);
   graphics$triangle$IIIIII(260, 372, 223, 446, 290, 422);
   graphics$fill$III(229, 176, 144);
   graphics$triangle$IIIIII(260, 372, 223, 446, 201, 393);
   graphics$fill$III(228, 166, 136);
   graphics$triangle$IIIIII(189, 348, 260, 372, 201, 393);
   graphics$fill$III(230, 173, 149);
   graphics$triangle$IIIIII(189, 348, 144, 354, 201, 393);
   graphics$fill$III(239, 213, 203);
   graphics$triangle$IIIIII(124, 372, 144, 354, 201, 393);
   graphics$fill$III(221, 163, 148);
   graphics$triangle$IIIIII(124, 372, 132, 401, 201, 393);
   graphics$fill$III(210, 143, 120);
   graphics$triangle$IIIIII(170, 423, 132, 401, 201, 393);
   graphics$fill$III(215, 150, 116);
   graphics$triangle$IIIIII(170, 423, 223, 446, 201, 393);
}
function main$main() {
   return graphics$p3$m00m00m00(function() {
      return main$init();
   }, function() {
      return main$draw();
   }, function() {
      return main$keyPressed();
   });
}
function main$init() {
   graphics$size$II(400, 400);
}
function main$draw() {
   graphics$background$III(0, 0, 0);
   graphics$line$IIII(0, 0, 400, 400);
}
function main$keyPressed() {
   graphics$size$II(graphics$width() + 1, graphics$height() + 1);
}
let myMain$x$static = 400;
function myMain$main() {
   graphics$size$II(100, 50);
   graphics$draw$f1v1T1v1Tv1T(function(p0) {
      return myMain$toDraw$Q5state(p0);
   }, new Wy.Record({circlex: 50, wallAx: 15, wallBx: 85, velx: 1}));
}
function myMain$toDraw$Q5state(s) {
   graphics$background$III(0, 0, 0);
   graphics$ellipse$IIII(s.circlex, 25, 20, 20);
   graphics$line$IIII(s.wallAx, 5, s.wallAx, 45);
   graphics$line$IIII(s.wallBx, 5, s.wallBx, 45);
   let velx = s.velx;
   if((s.circlex > s.wallBx) || (s.circlex < s.wallAx))  {
      velx = (-2) * velx;
   }
   return new Wy.Record({circlex: s.circlex + velx, velx: velx, wallAx: s.wallAx, wallBx: s.wallBx});
}
