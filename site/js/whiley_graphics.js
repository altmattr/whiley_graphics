var graphics = {}
graphics.canvas = document.createElement("canvas");
graphics.frameRate = 17;
graphics.currState = false;
graphics.canvasId = "whiley_graphics";
graphics.filling = true;
graphics.fill = "#FFFFFFF";
graphics.width = 100;
graphics.height = 100;
graphics.stroking = true;
graphics.stroke = "#FFFFFF";
graphics.strokeWeight = 1;
graphics.currentState = {};
graphics.draw =   function(toExecute) {
  graphics.currentState = toExecute(graphics.currentState);
  setTimeout(function() {
        graphics.draw(toExecute);
  }, graphics.frameRate);
};

graphics.keyPressHandler = function(toExecute){
  graphics.canvas.onkeypress = function(evt){
    graphics.currentState = toExecute(evt.key, graphics.currentState);
  };
}

function graphics$ellipse$IIII(x, y, w, h) {
  console.log("ellipse ran");
  var ctx = graphics.canvas.getContext("2d");
  ctx.beginPath();
  ctx.arc(x, y, w, 0, 2 * Math.PI);
  ctx.strokeStyle = graphics.stroke;
  ctx.fillStyle = graphics.fill;
  ctx.lineWidth = graphics.strokeWeight;
  if(graphics.filling){ctx.fill();}
  if(graphics.stroking){ctx.stroke();}
}

function graphics$triangle$IIIIII(x1, y1, x2, y2, x3, y3) {
    var ctx = graphics.canvas.getContext("2d");
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineTo(x3, y3);
    ctx.lineTo(x1, y1);
    ctx.strokeStyle = graphics.stroke;
    ctx.fillStyle = graphics.fill;
    ctx.lineWidth = graphics.strokeWeight;
    if(graphics.filling) {ctx.fill();}
    if(graphics.stroking) {ctx.stroke();}
}

  
function graphics$line$IIII(x1,y1,x2,y2) {
  var ctx = graphics.canvas.getContext("2d");
  ctx.beginPath();
  ctx.moveTo(x1,y1);
  ctx.lineTo(x2,y2);
  ctx.strokeStyle = graphics.stroke
  ctx.lineWidth = graphics.strokeWeight;
  if(graphics.stroking){ctx.stroke();}
}

function graphics$noStroke() {
    graphics.stroking = false;
}

function graphics$strokeWeight$I(w) {
  graphics.strokeWeight = w;
}

function graphics$noFill() {
    graphics.filling = false;
}

  
function graphics$fill$III(r,g,b){
  graphics.filling = true;
  graphics.fill = rgbToHex(r,g,b);
}

function graphics$rect$IIII(x,y,w,h) {
  var ctx = graphics.canvas.getContext("2d");
  ctx.strokeStyle = graphics.stroke
  ctx.fillStyle = graphics.fill
  ctx.lineWidth = graphics.strokeWeight;
  if(graphics.filling){ctx.fillRect(x,y,w,h);}
  if(graphics.stroking){ctx.drawRect(x,y,w,h);}
}

function graphics$size$II(x, y) {
  graphics.width = x;
  graphics.height = y;
  graphics.canvas.width = x;
  graphics.canvas.height = y;
}

function graphics$background$III(r,g,b) {
  var ctx = graphics.canvas.getContext("2d");
  ctx.fillStyle = rgbToHex(r,g,b);
  ctx.clearRect(0,0,graphics.canvas.width,graphics.canvas.height);
  ctx.fillRect(0,0,graphics.canvas.width,graphics.canvas.height);
}

function componentToHex(c){
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r,g,b){
  return "#" + componentToHex(r)+componentToHex(g)+componentToHex(b);
}


function graphics$frameRate$I(a){
  if(a > 0){
    graphics.frameRate = 1000/a;
  }
}

function graphics$draw$f1v1T1v1Tv1T(toExecute, vars){
  graphics.currentState = vars;
  graphics.draw(toExecute);
}

function graphics$keyPressHandler$f2Q4charv1T1v1T(handler) {
  graphics.keyPressHandler(handler);
}

function graphics$width(){
    return graphics.width;
}
function graphics$height(){
    return graphics.height;
}
function graphics$stroke$III(r,g,b){
    graphics.stroking = true;
    graphics.stroke = rgbToHex(r,g,b);
    console.log("stroke was called");
}
  
function graphics$p3$m00m00(init, draw){
  function drawIt(){
    console.log("drawing");
    draw();
    setTimeout(drawIt, graphics.frameRate);
  }
  init();
  drawIt();
  return graphics.canvas;
}

function graphics$p3$m00m00m00(init, draw, keyPress){
  graphics.canvas.setAttribute("tabindex", "1");
  graphics.canvas.onkeypress = function(){
    console.log("key pressed");
    keyPress();
  }
  return graphics$p3$m00m00(init, draw);
}