import char from std::ascii
import Node from w3c::dom

// old style
public native function draw<T>(function (T) -> T ret_state, T init_state)

// new style - "p3" means "processing"
public native function p3(method() setup, method() draw) -> Node
public native function p3(method() setup, method() draw, method() keyPressed) -> Node

public native function keyPressHandler<T>(function (char, T)-> T ret_state)
public native function size(int w, int h)
public native function ellipse(int x, int y, int w, int h)
public native function triangle(int x1, int y1, int x2, int y2, int x3, int y3)
public native function background(int r, int g, int b)
public native function rect(int x, int y, int w, int h)
public native function noStroke()
public native function strokeWeight(int w)
public native function stroke(int r, int g, int b)
public native function noFill()
public native function fill(int r, int g, int b)
public native function line(int x1, int y1, int x2, int y2)
public native function frameRate(int x)
public native function width() -> int
public native function height() -> int
