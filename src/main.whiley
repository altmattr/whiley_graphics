import              graphics
import Node    from w3c::dom

public function main() -> Node:
    return graphics::p3(&init, &draw, &keyPressed)

method init():
    graphics::size(400,400)

method draw():
    graphics::background(0,0,0)
    graphics::line(0,0,400,400)

method keyPressed():
    graphics::size(graphics::width()+1, graphics::height()+1)