import graphics

int x = 400 // needed to cover for parser error - see what happens when you take it out
type state is {int circlex, int velx, int wallAx, int wallBx}

method main():
    graphics::size(100,50)
    graphics::draw(&toDraw, {circlex:50, wallAx:15, wallBx:85, velx:1})

function toDraw(state s) -> state
    requires s.circlex < s.wallBx
    requires s.circlex > s.wallAx
    ensures s.circlex < s.wallBx
    ensures s.circlex > s.wallAx:


    graphics::background(0, 0, 0)
    graphics::ellipse(s.circlex, 25, 20, 20)
    graphics::line(s.wallAx, 5, s.wallAx, 45)
    graphics::line(s.wallBx, 5, s.wallBx, 45)

    int velx = s.velx
    if (s.circlex > s.wallBx || s.circlex < s.wallAx):
        velx = -2*velx
        
       
    return {circlex: s.circlex+velx, velx: velx, wallAx: s.wallAx, wallBx: s.wallBx}