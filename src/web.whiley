/*
import string from std::ascii

type command is function() -> message

type message is (string s) where s == "Done" || s == "Not Done"
message Done = "Done"
message NotDone = "NotDone"

type model is int

type attribute is {
    string attr, 
    string | message value
}

type node is {
    string name,
    attribute[] attrs, 
    node[] children
} | string

function noNodes() -> node[]:
	return []

native function playApp(model m,
					  command[] c,
					  function (model) -> node n
					  //function (message) -> (model, command[])mc
					  )

method main():
	model mod
	command[] comms
	mod, comms = init()
	playApp(mod, comms, &view)

function init() -> (model m, command[] cs):
	return 1, []

function view(model m) -> node:
	return {name: "text", attrs: [{attr: "onClick", value: Done}], children: noNodes()}

function update(model m) -> (model rm, command[] cs):
	return m, []

*/