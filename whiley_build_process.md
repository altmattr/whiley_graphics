# Summary

  * To build your own whiley application, you need to use the `wy` command (most likely `wy build`).
  * To get `wy` you need the Whiley Development Kit and you need to have both WHILEYHOME and PATH set appropriately.
  * There are three ways to get the WDK.  You can download a current version, build a fresh distribution, or build a "bleeding edge" version.

# Downloading an existing WDK
Clone or download the repository.  You will see a `dist` directory with various WDK versions.  Pick the latest and point your WHILEYHOME and PATH directories to it.  For example, on my machine, with the repository at `/home/mattr/repos/WDK` I needed the following to setup the 0.5.0 wdk.

~~~~~
export WHILEYHOME=/home/mattr/repos/WhileyDevelopmentKit/dist/wdk-v0.5.0
export PATH=/home/mattr/repos/WhileyDevelopmentKit/dist/wdk-v0.5.0/bin:$PATH
~~~~~

# Build a fresh distribution

WDK will try to compile a set of libraries from maven based on what you tell it you need.  If you have cloned the repository you will see a `config.xml` file in the root of the repository.  This is where you configure what versions of what components you want.  Having done that, run `ant build`.  If this works, you will see a new directory in the `dist` directory which is your distrubtion.  Now set WHILEYHOME and PATH as you would for _Downloading an existing WDK_.

# Build a bleeding edge version

If the components you want are not in maven central, you will have to add them to your local maven.

The WDK will look for (at least) each of the following components:

  * Whiley Compiler Collection (wycc)
  * Whiley Compiler (wyc)
  * Whiley Theorem Prover (wytp)
  * Whiley to JavaScript Compiler (wyjs)

Thus, for each of these in order, you should:
 
  * clone the respository
  * checkout branch you want (`git checkout develop`)
  * double-check all the dependencies
  * (optionally) you can test the project with `mvn test`
  * install the project locally (`mvn install -DskipTests -Dgpg.skip -Dmaven.javadoc.skip=true`)

Once that is done, you have local versions of all the maven dependencies and you can go back and follow the steps for _Build a fresh distribution_.